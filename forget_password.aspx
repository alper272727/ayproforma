﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="forget_password.aspx.cs" Inherits="ayproforma.forget_password" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>PDA Calculate</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function email_not_match() {
            swal({
                title: 'Reset Password!',
                text: 'Oops! E-Mail did not find.',
                type: 'error'
            });
        }
        function email_not_blank() {
            swal({
                title: 'Reset Password!',
                text: 'Oops! The email cannot be blank',
                type: 'error'
            });
        }
        function success_send_email() {
            swal({
                title: 'Reset Password!',
                text: 'Activation code was send succesfully',
                type: 'success'
            });
        }
        function success_password() {
            swal({
                title: 'Change Password!',
                text: 'Password has changed succesfully',
                type: 'success'
            });
        }
        function alert_password() {
            swal({
                title: 'Change Password!',
                text: 'Oops! There is an error. Contact with admin, please',
                type: 'error'
            });
        }
        function alert_password_invalid() {
            swal({
                title: 'Change Password!',
                text: 'Oops! Invalid password',
                type: 'error'
            });
        }

        function alert_password_wrong() {
            swal({
                title: 'Change Password!',
                text: 'Oops! Password is not matching.',
                type: 'error'
            });
        }
        function invalid_act_code() {
            swal({
                title: 'Activation Code!',
                text: 'Activation Code is invalid .',
                type: 'error'
            });
        }
        function success_password() {
            swal({
                title: 'Change Password!',
                text: 'Password has changed succesfully',
                type: 'success'
            });
        }
        function alert_password() {
            swal({
                title: 'Change Password!',
                text: 'Oops! There is an error. Contact with admin, please',
                type: 'error'
            });
        }
        function alert_password_invalid() {
            swal({
                title: 'Change Password!',
                text: 'Oops! Invalid password',
                type: 'error'
            });
        }

        function alert_password_wrong() {
            swal({
                title: 'Change Password!',
                text: 'Oops! Password is not matching.',
                type: 'error'
            });
        }

        
        function success_send_act() {
            swal({
                title: "Reset Password!",
                text: "Activate code is sending.",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            },
function () {
    setTimeout(function () {
        swal("Activate code sent successfully!");
    }, 2000);
});
        }

        </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
      <!-- Header -->
        <header id="header">
            <div class="inner">
                <a href="home_page.aspx" class="logo"><strong>PDA Calculator</strong> by AYPROFORMA</a>
                <nav id="nav">
                    <a href="home_page.aspx">Home Page</a>
                    <a href="login.aspx">Log in</a>
                    <a href="#">Our Mission</a>
                    <a href="#">Our Vision</a>
                    <a href="#">Who We Are?</a>
                </nav>
                <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
            </div>
        </header>

        <!-- Banner -->
          <section id="banner">
            <div class="inner">
                <h3>Reset Password</h3>
                <!--Email Yeri -->
                <asp:Panel ID="before_activation_code" runat="server">   
                    <div class="login half first ">
                        <label for="user_name" style="text-align:left" >E-Mail<asp:TextBox ID="txt_email"  
                            CssClass="main" placeholder="E-Mail" TextMode="Email" runat="server" style="text-align:left"></asp:TextBox>
                        </label>  
                        &nbsp;
                    </div>        
                    <ul class="login">   
                        <li>
                            <asp:Button ID="btn_email" style="text-align:center" OnClick="btn_email_Click" CssClass="main" runat="server" Text="Reset Password" Height="49px" Width="224px"/>
                        </li>
                    </ul> 
                </asp:Panel>
                
                 <!--Activation Code Yeri -->
                <asp:Panel ID="activation_code" runat="server">   
                    &nbsp;
                    &nbsp;
                    <div class="login half first ">
                                <asp:TextBox ID="txt_act_code"  
                            CssClass="main" style="text-align:center" placeholder="E-Mail" Font-Size="X-Large" TextMode="Number" runat="server"></asp:TextBox>
                    </div>        
                    &nbsp;
                    &nbsp;
                    <ul class="login">   
                        <li>
                            <asp:Button ID="btn_act_code"  style="text-align:center" OnClick="btn_act_code_Click" CssClass="main" runat="server" Text="Confirm" Height="49px" Width="224px"/>
                        </li>
                    </ul> 
                </asp:Panel>

                 <!--Şifre Değiştirme Yeri -->
                <asp:Panel ID="confirm_password" runat="server">   
                    <div class="login half first ">
                        <label for="user_name" style="text-align:left" >New Password <asp:TextBox ID="txt_new_password"  
                            CssClass="main" placeholder="Password" TextMode="Password" runat="server" style="text-align:left"></asp:TextBox>
                        </label>  
                        &nbsp;
                        <label for="user_name" style="text-align:left" >New Re-Password <asp:TextBox ID="txt_new_password_confirm"  
                            CssClass="main" placeholder="Re-Password" TextMode="Password" runat="server" style="text-align:left"></asp:TextBox>
                        </label>  
                        &nbsp;
                    </div>        
                    <ul class="login">   
                        <li>
                          <asp:Button ID="btn_change_password" style="text-align:center" OnClick="btn_change_password_Click" CssClass="main" runat="server" Text="Change Password" Height="49px" Width="224px"/>
                        </li>
                    </ul> 
                </asp:Panel>
            </div>
        </section>
    </div>
    </form>
</body>
</html>
