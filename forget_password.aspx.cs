﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Data;
using System.Drawing;

namespace ayproforma
{
    public partial class forget_password : System.Web.UI.Page
    {
        static String activitioncode;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[0].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            before_activation_code.Visible = true;
            activation_code.Visible = false;
            confirm_password.Visible = false;

        }

        protected void btn_email_Click(object sender, EventArgs e)
        {   


            if (txt_email.Text != "")
            {
                string sorgu = "SELECT * FROM [PDA_CALCULATION].[dbo].[users] where Email= @email";
                string email = txt_email.Text;
                SqlCommand cmd = new SqlCommand(sorgu, con);
                cmd.Parameters.AddWithValue("@email", email);

                try
                {
                    
                    
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    
                    if (dr.Read())
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_send_act();", true);
                        Random random = new Random();
                        activitioncode = random.Next(1001, 9999).ToString();
                        con.Close();

                        string update_activitioncode = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Act_Code_Reset ='" + activitioncode + "' where Email = @email ";                        
                        sendcode();
                        string email2 = txt_email.Text;
                        con.Open();
                        SqlCommand cmd_act = new SqlCommand(update_activitioncode, con);
                        cmd_act.Parameters.AddWithValue("@email", email2);
                        cmd_act.ExecuteNonQuery();
                        
                        before_activation_code.Visible = false;
                        activation_code.Visible = true;
                        confirm_password.Visible = false;

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "email_not_match();", true);
                        before_activation_code.Visible = true;
                        activation_code.Visible = false;
                        confirm_password.Visible = false;
                    }
                    con.Close();


                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "email_not_match();", true);

                }
                con.Close();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "email_not_blank();", true);
            }           
            }

        protected void btn_pasword_confirm(object sender, EventArgs e)
        {

            if (txt_new_password.Text == txt_new_password_confirm.Text)
            {
                string update_password = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Password ='" + txt_new_password.Text + "' where Email = @email ";
                    SqlCommand cmd_password = new SqlCommand(update_password, con);
                    con.Open();
                    try
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_password();", true);
                        string email3 = txt_email.Text;
                        cmd_password.Parameters.AddWithValue("@email", email3);
                        cmd_password.ExecuteNonQuery();
                        con.Close();
                        before_activation_code.Visible = true;
                        activation_code.Visible = false;
                        confirm_password.Visible = true;
                    }
                    catch (Exception)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_password();", true);
                        before_activation_code.Visible = false;
                        activation_code.Visible = false;
                        confirm_password.Visible = true;

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_password_wrong();", true);
                    before_activation_code.Visible = false;
                    activation_code.Visible = false;
                    confirm_password.Visible = true;
                }
            con.Close();
            }
                     

           
        private void sendcode()
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.Credentials = new System.Net.NetworkCredential("ylmzalper9427@gmail.com", "Alper272727.");
            smtp.EnableSsl = true;
            MailMessage msg = new MailMessage();
            msg.Subject = "Activation Code to Verify Email Adress";
            msg.Body = "Dear " + txt_email.Text+ ",\n\n" + "Your Activation code is " + activitioncode + "\n\n\nThanks & Best Regards" + "\nAYPROFORMA";
            string toaddres = txt_email.Text;
            msg.To.Add(toaddres);
            string fromaddress = "ylmzalper9427@gmail.com";
            msg.From = new MailAddress(fromaddress);

            try
            {
                smtp.Send(msg);
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void btn_act_code_Click(object sender, EventArgs e)
        {


            String activation_code2;
            string sorgu_activation_code = "SELECT Act_Code_Reset FROM [PDA_CALCULATION].[dbo].[users] where Email= @email";
            SqlCommand cmd_password_reset = new SqlCommand(sorgu_activation_code, con);
            string email4 = txt_email.Text;
            cmd_password_reset.Parameters.AddWithValue("@email", email4);
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd_password_reset;
            DataSet ds = new DataSet();
            da.Fill(ds);
            SqlDataReader dr = cmd_password_reset.ExecuteReader();
            activation_code2 = ds.Tables[0].Rows[0]["Act_Code_Reset"].ToString();

            if (activation_code2 == txt_act_code.Text)
            {
                con.Close();
                before_activation_code.Visible = false;
                activation_code.Visible = false;
                confirm_password.Visible = true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_act_code();", true);
            }
        }

        protected void btn_change_password_Click(object sender, EventArgs e)
        {
            if (txt_new_password.Text == txt_new_password_confirm.Text)
            {
                string update_password = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Password ='" + txt_new_password.Text + "' where Email= @email ";
                SqlCommand cmd_password = new SqlCommand(update_password, con);
                con.Open();
                try
                {
                    string email5 = txt_email.Text;
                    cmd_password.Parameters.AddWithValue("@email", email5);
                    cmd_password.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_password();", true);
                    before_activation_code.Visible = true;
                    activation_code.Visible = false;
                    confirm_password.Visible = false;
                    Response.Redirect("login.aspx");
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_password();", true);
                    before_activation_code.Visible = false;
                    activation_code.Visible = false;
                    confirm_password.Visible = true;

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_password_invalid();", true);
                before_activation_code.Visible = false;
                activation_code.Visible = false;
                confirm_password.Visible = true;
            }
            con.Close();
        }
        }
        }