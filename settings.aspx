﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="settings.aspx.cs" Inherits="ayproforma.settings" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDA Calculate</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <script src="https://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
   
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.css"
        rel="stylesheet" type="text/css" />

    <script>
        var a;
     </script>
    <script type="text/javascript">

        function success_password() {
            swal({
                title: 'Change Password!',
                text: 'Password has changed succesfully',
                type: 'success'
            });
        }
        function alert_password() {
            swal({
                title: 'Change Password!',
                text: 'Oops! There is an error. Contact with admin, please',
                type: 'error'
            });
        }
        function alert_password_invalid() {
            swal({
                title: 'Change Password!',
                text: 'Oops! Invalid password',
                type: 'error'
            });
        }

        function alert_password_wrong() {
            swal({
                title: 'Change Password!',
                text: 'Oops! Password is not matching.',
                type: 'error'
            });
        }
        function alert_changed_name_surname() {
            swal({
                title: 'Name-Surname!',
                text: 'Oops! Invalid name-surname.',
                type: 'error'
            });
        }

        function success_changed_name_surname() {
            swal({
                title: 'Name-Surname!',
                text: 'Changed name-surname.',
                type: 'success'
            });
        }
        function alert_changed_email() {
            swal({
                title: 'E-Mail!',
                text: 'Oops! You write same e-mail address.',
                type: 'error'
            });
        }
        function alert_invalid_email() {
            swal({
                title: 'E-Mail!',
                text: 'Oops! Invalid e-mail address.',
                type: 'error'
            });
        }
        function success_changed_email() {
            swal({
                title: 'E-Mail!',
                text: 'Your e-mail changed .',
                type: 'success'
            });
        }
        function invalid_act_code() {
            swal({
                title: 'Activation Code!',
                text: 'Activation Code is invalid .',
                type: 'error'
            });
        }
        function invalid_update_email() {
            swal({
                title: 'E-Mail!',
                text: 'Email cannot be change. Please contact with admin.',
                type: 'error'
            });
        }

        function act_code_send_email() {
            var value = document.getElementById('<%=txt_email_edit.ClientID%>').value;
            swal({
                title: 'E-Mail!',
                text: 'Activation code sent to '+ value,
                type: 'success'
            });
        }
        function success_changed_home_phone() {
            swal({
                title: 'Home Phone!',
                text: 'Changed home phone',
                type: 'success'
            });
        }
        function alert_changed_home_phone() {
            swal({
                title: 'Home Phone!',
                text: 'Home phone did not change',
                type: 'error'
            });
        }
        function invalid_changed_home_phone() {
            swal({
                title: 'Home Phone!',
                text: 'Invalid home phone',
                type: 'error'
            });
        }
        function success_changed_mobile_phone() {
            swal({
                title: 'Mobile Phone!',
                text: 'Changed mobile phone',
                type: 'success'
            });
        }
        function alert_changed_mobile_phone() {
            swal({
                title: 'Mobile Phone!',
                text: 'Mobile phone did not change',
                type: 'error'
            });
        }
        function invalid_changed_mobile_phone() {
            swal({
                title: 'Mobile Phone!',
                text: 'Invalid mobile phone',
                type: 'error'
            });
        }

        function success_changed_company() {
            swal({
                title: 'Company!',
                text: 'Changed company',
                type: 'success'
            });
        }
        function alert_changed_company() {
            swal({
                title: 'Company!',
                text: 'Company did not change',
                type: 'error'
            });
        }
        function invalid_changed_company() {
            swal({
                title: 'Company!',
                text: 'Invalid company',
                type: 'error'
            });
        }
        function success_changed_country() {
            swal({
                title: 'Country!',
                text: 'Changed country',
                type: 'success'
            });
        }
        function alert_changed_country() {
            swal({
                title: 'Country!',
                text: 'Country did not change',
                type: 'error'
            });
        }
        function invalid_changed_country() {
            swal({
                title: 'Country!',
                text: 'Invalid country',
                type: 'error'
            });
        }
        function success_changed_city() {
            swal({
                title: 'City!',
                text: 'Changed city',
                type: 'success'
            });
        }
        function alert_changed_city() {
            swal({
                title: 'City!',
                text: 'City did not change',
                type: 'error'
            });
        }
        function invalid_changed_city() {
            swal({
                title: 'City!',
                text: 'Invalid city',
                type: 'error'
            });
        }
        function success_changed_district() {
            swal({
                title: 'District!',
                text: 'Changed district',
                type: 'success'
            });
        }
        function alert_changed_district() {
            swal({
                title: 'District!',
                text: 'District did not change',
                type: 'error'
            });
        }
        function invalid_changed_district() {
            swal({
                title: 'District!',
                text: 'Invalid district',
                type: 'error'
            });
        }
        function success_changed_street() {
            swal({
                title: 'Street!',
                text: 'Changed street',
                type: 'success'
            });
        }
        function alert_changed_street() {
            swal({
                title: 'Street!',
                text: 'Street did not change',
                type: 'error'
            });
        }
        function invalid_changed_street() {
            swal({
                title: 'Street!',
                text: 'Invalid street',
                type: 'error'
            });
        }
        function success_changed_office() {
            swal({
                title: 'Office!',
                text: 'Changed office',
                type: 'success'
            });
        }
        function alert_changed_office() {
            swal({
                title: 'Office!',
                text: 'Office did not change',
                type: 'error'
            });
        }
        function invalid_changed_office() {
            swal({
                title: 'Office!',
                text: 'Invalid office',
                type: 'error'
            });
        }
        function success_changed_no() {
            swal({
                title: 'No!',
                text: 'Changed no',
                type: 'success'
            });
        }
        function alert_changed_no() {
            swal({
                title: 'No!',
                text: 'No did not change',
                type: 'error'
            });
        }
        function invalid_changed_no() {
            swal({
                title: 'No!',
                text: 'Invalid no',
                type: 'error'
            });
        }
        function invalid_changed_logo() {
            swal({
                title: 'Logo!',
                text: 'Opps! Please select an image from your computer',
                type: 'error'
            });
        }
        function success_changed_logo() {
            swal({
                title: 'Logo!',
                text: 'Changed logo',
                type: 'success'
            });
        }
        function alert_changed_logo() {
            swal({
                title: 'Logo!',
                text: 'Logo did not change',
                type: 'error'
            });
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>

            <!-- Header -->
            <header id="header">
                <div class="inner">
                    <a href="home_page.aspx" class="logo"><strong>PDA Calculator</strong> by AYPROFORMA</a>
                    <asp:Panel ID="after_login_panel" runat="server">
                        Welcome
                        <asp:LinkButton ID="lbl_login" CssClass="main" OnClick="lbl_login_Click" Font-Bold="true" ForeColor="#6cc091" runat="server"></asp:LinkButton>
                    </asp:Panel>
                    <nav id="nav">
                        <a href="home_page.aspx">Home Page</a>
                        <a href="home_page.aspx">My Birds</a>
                        <a href="home_page.aspx">Settings</a>
                        <asp:LinkButton ID="btn_log_out" OnClick="btn_log_out_Click" runat="server">Log out</asp:LinkButton>
                    </nav>
                    <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
                </div>
            </header>



            <!-- Banner -->
            <section id="banner">
                <div class="inner">
                    <header>
                        <h1>Welcome to PDA Calculator</h1>
                    </header>
                    <div class="flex ">
                        <div>
                            <span class="icon fas fa-user"></span>
                            <h3>Account Settings</h3>
                            <p>Account Settings changed</p>
                        </div>
                    </div>
                </div>
            </section>


            <section id="three" class="wrapper align-center">
                <div class="banner">
                    <h3>Account Settings</h3>
                    <table>
                        <!--Kullanıcı Adı-->
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label1" runat="server" Text="User Name:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_user_name" runat="server" Text="User Name"></asp:Label>
                                </td>
                            </tr>
                        <!--Şifre Edit-->
                        <asp:Panel ID="before_password_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label2" runat="server" Text="Password:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_password" runat="server" Text="***"></asp:Label>
                                    <asp:LinkButton ID="LinkButton3" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_password_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_password_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label15" runat="server" Text="Password:"></asp:Label>
                                    <asp:TextBox ID="txt_old_password" Width="200px" Height="28px" Font-Bold="true" placeholder="Old Password" Style="display: inline; text-align: center;"
                                         runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:TextBox ID="txt_password" Width="200px" Height="28px" Font-Bold="true" placeholder="New Password" Style="display: inline; text-align: center;"
                                         runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:TextBox ID="txt_password2" Width="200px" Height="28px" Font-Bold="true" placeholder="New Re-Password" Style="display: inline; text-align: center;"
                                         runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:LinkButton ID="LinkButton13" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_password_edit_ok">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton14" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_password_edit_cancel">
                                    </asp:LinkButton>
                                    <asp:Label ID="lbl_password_text" runat="server" ForeColor="black" BackColor="Red" BorderColor="White" Font-Bold="true" Font-Size="Medium"></asp:Label>
                                </td>
                            </tr>
                        </asp:Panel>
                        
                        <!--Ad-Soyad Edit-->
                        <asp:Panel ID="before_name_surname_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label3" runat="server" Text="Name-Surname:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_name_surname" runat="server" Text="Name-Surname"></asp:Label>
                                    <asp:LinkButton ID="LinkButton1" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_name_surname_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_name_surname_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label16" runat="server" Text="Name-Surname:"></asp:Label>
                                    <asp:TextBox ID="txt_name_edit" Width="200px" Height="28px" Font-Bold="true" placeholder="Name" Style="display: inline; text-align: center;"
                                         runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txt_surnmame_edit" Width="200px" Height="28px" Font-Bold="true" placeholder="Surname" Style="display: inline; text-align: center;"
                                         runat="server"></asp:TextBox>
                                    <asp:LinkButton ID="LinkButton15" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_name_surname_edit_ok">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton16" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_name_surname_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>

                        <!--E-Mail Edit-->
                        <asp:Panel ID="before_email_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label4" runat="server" Text="E-Mail:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_email_edit" runat="server" Text="E-Mail"></asp:Label>
                                    <asp:LinkButton ID="LinkButton2" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_email_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_email_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label17" runat="server" Text="E-Mail:"></asp:Label>
                                    <asp:TextBox ID="txt_email_edit" Width="300px" Height="28px" Font-Bold="true" placeholder="E-Mail" Style="display: inline; text-align: center;"
                                         TextMode="Email" runat="server"
                                        ></asp:TextBox>                     
                                    <asp:LinkButton ID="LinkButton17" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px" 
                                        OnClientClick ="act_code_send_email()"
                                        OnClick="btn_email_edit_ok"
                                         >
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton18" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_email_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="confirm_after_email_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label13" runat="server" Text="E-Mail:"></asp:Label>
                                    <asp:TextBox ID="txt_activation_code" Width="200px" Height="28px" Font-Bold="true" placeholder="Activation Code" Style="display: inline; text-align: center;"
                                         TextMode="Number" runat="server"></asp:TextBox>                       
                                    <asp:LinkButton ID="LinkButton12" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="50" OnClick="btn_confirm_email_edit_ok" >
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton38" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_confirm_email_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                         <!--Home Phone Edit-->
                        <asp:Panel ID="before_home_phone_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label5" runat="server" Text="Home Phone:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_home_phone" runat="server" Text="Home Phone"></asp:Label>
                                    <asp:LinkButton ID="LinkButton10" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_home_phone_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_home_phone_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label18" runat="server" Text="Home Phone:"></asp:Label>
                                    <asp:TextBox ID="txt_home_phone" Width="200px" Height="28px" Font-Bold="true" placeholder="Home Phone" Style="display: inline; text-align: center;"
                                         TextMode="Phone" runat="server"></asp:TextBox>                     
                                    <asp:LinkButton ID="LinkButton19" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_home_phone_edit_ok">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton20" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_home_phone_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                         <!--Mobile Phone Edit-->
                        <asp:Panel ID="before_mobile_phone_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label6" runat="server" Text="Mobile Phone:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_mobile_phone" runat="server" Text="Mobile Phone"></asp:Label>
                                    <asp:LinkButton ID="LinkButton11" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_mobile_phone_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_mobile_phone_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label19" runat="server" Text="Mobile Phone:"></asp:Label>
                                    <asp:TextBox ID="txt_mobile_phone" Width="200px" Height="28px" Font-Bold="true" placeholder="Mobile Phone" Style="display: inline; text-align: center;"
                                         TextMode="Phone" runat="server"></asp:TextBox>                     
                                    <asp:LinkButton ID="LinkButton21" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_mobile_phone_edit_ok">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton22" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_mobile_phone_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                         <!--Company Edit-->
                        <asp:Panel ID="before_company_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label7" runat="server" Text="Company:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_company" runat="server" Text="Company"></asp:Label>
                                    <asp:LinkButton ID="LinkButton4" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_company_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_company_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label20" runat="server" Text="Company:"></asp:Label>
                                    <asp:TextBox ID="txt_company" Width="200px" Height="28px" Font-Bold="true" placeholder="Company" Style="display: inline; text-align: center;"
                                         runat="server"></asp:TextBox>                     
                                    <asp:LinkButton ID="LinkButton23" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_company_edit_ok">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton24" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_company_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                         <!--Country Edit-->
                        <asp:Panel ID="before_country_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label14" runat="server" Text="Country:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_country" runat="server" Text="Country"></asp:Label>
                                    <asp:LinkButton ID="LinkButton35" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_country_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_country_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label26" runat="server" Text="Country:"></asp:Label>
                                    <asp:TextBox ID="txt_country" Width="200px" Height="28px" Font-Bold="true" placeholder="Country" Style="display: inline; text-align: center;"
                                         runat="server"></asp:TextBox>                     
                                    <asp:LinkButton ID="LinkButton36" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_country_edit_ok">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton37" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_country_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                         <!--City Edit-->
                        <asp:Panel ID="before_city_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label8" runat="server" Text="City:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_city" runat="server" Text="City"></asp:Label>
                                    <asp:LinkButton ID="LinkButton5" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_city_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_city_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label21" runat="server" Text="City:"></asp:Label>
                                    <asp:TextBox ID="txt_city" Width="200px" Height="28px" Font-Bold="true" placeholder="Country" Style="display: inline; text-align: center;"
                                         runat="server"></asp:TextBox>                     
                                    <asp:LinkButton ID="LinkButton25" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_city_edit_ok">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton26" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_city_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <!--District Edit-->
                        <asp:Panel ID="before_district_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label9" runat="server" Text="District:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_district" runat="server" Text="District"></asp:Label>
                                    <asp:LinkButton ID="LinkButton6" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_district_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_district_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label22" runat="server" Text="District:"></asp:Label>
                                    <asp:TextBox ID="txt_district" Width="200px" Height="28px" Font-Bold="true" placeholder="District" Style="display: inline; text-align: center;"
                                         runat="server"></asp:TextBox>                     
                                    <asp:LinkButton ID="LinkButton27" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_district_edit_ok">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton28" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_district_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <!--Street Edit-->
                        <asp:Panel ID="before_street_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label10" runat="server" Text="Street:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_street" runat="server" Text="Street"></asp:Label>
                                    <asp:LinkButton ID="LinkButton7" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_street_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_street_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label23" runat="server" Text="Street:"></asp:Label>
                                    <asp:TextBox ID="txt_street" Width="200px" Height="28px" Font-Bold="true" placeholder="Street" Style="display: inline; text-align: center;"
                                         runat="server"></asp:TextBox>                     
                                    <asp:LinkButton ID="LinkButton29" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_street_edit_ok">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton30" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_street_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <!--Office Edit-->
                        <asp:Panel ID="before_office_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label11" runat="server" Text="Office:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_office" runat="server" Text="Office"></asp:Label>
                                    <asp:LinkButton ID="LinkButton8" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_office_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_office_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label25" runat="server" Text="Office:"></asp:Label>
                                    <asp:TextBox ID="txt_office" Width="200px" Height="28px" Font-Bold="true" placeholder="Office" Style="display: inline; text-align: center;"
                                         runat="server"></asp:TextBox>                     
                                    <asp:LinkButton ID="LinkButton31" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_office_edit_ok">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton32" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_office_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <!--No Edit-->
                        <asp:Panel ID="before_no_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label12" runat="server" Text="No:"></asp:Label>
                                    &nbsp&nbsp&nbsp<asp:Label ID="lbl_no" runat="server" Text="No"></asp:Label>
                                    <asp:LinkButton ID="LinkButton9" CssClass="fa fa-pencil"
                                        runat="server" OnClick="btn_before_no_edit"
                                        Height="28px" Width="28px"></asp:LinkButton>
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="after_no_edit" runat="server">
                            <tr>
                                <td>&nbsp&nbsp&nbsp<asp:Label ID="label27" runat="server" Text="No:"></asp:Label>
                                    <asp:TextBox ID="txt_no" Width="200px" Height="28px" Font-Bold="true" placeholder="Office" Style="display: inline; text-align: center;"
                                          TextMode="Number" runat="server"></asp:TextBox>                     
                                    <asp:LinkButton ID="LinkButton33" CssClass="icon fas fa-check" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_no_edit_ok">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton34" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_no_edit_cancel">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                            </asp:Panel>

                            <!--Upload Image-->
                        <asp:Panel ID="before_logo_edit" runat="server">
                            <tr>
                                <td>
                                    <asp:Image ID="old_image" Width="250px" Height="250px" runat="server"  />
                                    <asp:LinkButton ID="LinkButton39" CssClass="fa fa-pencil" Font-Size="XX-Large" runat="server"
                                        Height="56px" Width="56px"
                                        OnClick="before_btn_logo_edit">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                    </asp:Panel>
                        <asp:Panel ID="after_logo_edit" runat="server">
                            <tr>
                                <td>
                                    <asp:Image ID="new_image" Width="250px" Height="250px" runat="server"  />
                                    <asp:FileUpload ID="logo_upload" runat="server" />
                                    <asp:LinkButton ID="LinkButton40" CssClass="icon fas fa-upload" Font-Size="XX-Large" runat="server"
                                        Height="56px" Width="56px"
                                        OnClick="after_btn_logo_edit">
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton41" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                        Height="28px" Width="28px"
                                        OnClick="btn_logo_edit_cancel" Font-Size="XX-Large">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                    </asp:Panel>      
                    </table>
                </div>
            </section>


            <!-- Footer -->
            <footer id="footer">
                <div class="inner">
                    <h3>Contact with Us</h3>
                    <div class="field half first">
                        <!--Ad-Soyad-->
                        <label for="name">Name</label>
                        <asp:TextBox ID="txt_name" CssClass="main" placeholder="Name" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="required1"
                            ErrorMessage="Name is required"
                            ControlToValidate="txt_name"
                            Text="Name is required"
                            ForeColor="Red"
                            ValidationGroup="Group1"
                            runat="server" />
                        <label for="surname">Surname</label>
                        <asp:TextBox ID="txt_surname" CssClass="main" placeholder="Surname" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                            ErrorMessage="Surname is required"
                            ControlToValidate="txt_surname"
                            Text="Surname is required"
                            ForeColor="Red"
                            ValidationGroup="Group1"
                            runat="server" />
                    </div>
                    <!--Mail Telefon-->
                    <div class="field half">
                        <label for="telefon">Mobile Phone</label>
                        <asp:TextBox ID="txt_telefon" CssClass="main" placeholder="Exp:05XX-XXX-XX-XX" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="required2"
                            ErrorMessage="Phone is required"
                            ControlToValidate="txt_telefon"
                            Text="Phone is required"
                            ForeColor="Red"
                            Style="text-align: start;"
                            ValidationGroup="Group1"
                            runat="server" />
                        <label for="email">E-Mail</label>
                        <asp:TextBox ID="txt_email" CssClass="main" placeholder="E-Mail" TextMode="Email" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                            ErrorMessage="E-Mail is required"
                            ControlToValidate="txt_email"
                            Text="E-Mail is required"
                            ForeColor="Red"
                            Style="text-align: start;"
                            ValidationGroup="Group1"
                            runat="server" />
                    </div>
                    <div class="field">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" rows="6" placeholder="Please fill the all blanks for register."></textarea>
                    </div>
                    <!--Send-->
                    <ul class="actions">
                        <li>
                            <asp:Button ID="btn_send" CssClass="button alt" runat="server" Text="Send" OnClick="btn_send_Click" ValidationGroup="Group1" /></li>
                    </ul>
                    <asp:Label ID="lbl_send_contact_info" runat="server" ForeColor="black" BackColor="White" BorderColor="White" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                    <asp:Label ID="lbl_ip" runat="server" Text=""></asp:Label>
                    <div class="copyright">
                        &copy; AYPROFORMA
                    </div>
                </div>
            </footer>

            <!-- Scripts -->
            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>
        </div>
    </form>
</body>
</html>
