﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="ayproforma.login" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>PDA Calculate</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function login_not_match() {
            swal({
                title: 'Login!',
                text: 'Oops! The user name or password did not match our records. Please try again',
                type: 'error'
            });
        }
        function login_not_blank() {
            swal({
                title: 'Login!',
                text: 'Oops! The user name or password cannot be blank',
                type: 'error'
            });
        }
        function success_send_contact() {
            swal({
                title: 'Contact Form!',
                text: 'Contact Info was send succesfully',
                type: 'success'
            });
        }
        function alert_send_contact() {
            swal({
                title: 'Contact Form!',
                text: 'Oops! Contact Info was not send.',
                type: 'error'
            });
        } 
        </script>
</head>

<body>
    <form id="form1" runat="server">
    <div>
        <!-- Header -->
        <header id="header">
            <div class="inner">
                <a href="home_page.aspx" class="logo"><strong>PDA Calculator</strong> by AYPROFORMA</a>
                <nav id="nav">
                    <a href="home_page.aspx">Home Page</a>
                    <a href="login.aspx">Log in</a>
                    <a href="#">Our Mission</a>
                    <a href="#">Our Vision</a>
                    <a href="#">Who We Are?</a>
                </nav>
                <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
            </div>
        </header>

        <!-- Banner -->
        
        <section id="banner">
            
            <div class="inner">
                <asp:Panel ID="before_login" runat="server">
                    
                <h3>Log in</h3> 
                    <div class="login half first ">
                        <label for="user_name" style="text-align:left" aria-setsize="20">User Name<asp:TextBox ID="txt_user_name"  
                            CssClass="main" placeholder="User Name" runat="server" style="text-align:left"></asp:TextBox>
                        </label>  
                        &nbsp;
                    </div>
                     
                    <div class="login half">
                        <label for="password" style="text-align:left">Password<asp:TextBox ID="txt_password" 
                            TextMode="Password" CssClass="main" placeholder ="Password"
                            runat="server" style="text-align:left"></asp:TextBox>
                        </label>
                        &nbsp;</div>
                    <ul class="login">   
                        <li>
                            <asp:Button ID="btn_login"  CssClass="main" runat="server" Text="Log in" Height="49px" OnClick="btn_login_Click" Width="224px" />
                        </li>
                    </ul>
                    <ul class="login">   
                        <li>
                            <asp:LinkButton ID="forgot_pasword" ForeColor="Blue" Font-Size="Large" 
                               OnClick="forgot_pasword_Click" runat="server">Forgot Password</asp:LinkButton>
                        </li>
                    </ul>


                </asp:Panel>
            </div>
        </section>
 <!-- Footer -->
        <footer id="footer">
            <div class="inner">
            <h3>Contact with Us</h3>
                <div class="field half first">
                    <!--Ad-Soyad-->
                    <label for="name">Name</label>
                    <asp:TextBox ID="txt_name" CssClass ="main" placeholder="Name" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator id="required1" 
                        ErrorMessage="Name is required" 
                        ControlToValidate="txt_name" 
                        Text="Name is required" 
                        ForeColor="Red"
                        ValidationGroup="Group1"  
                        runat="server" />
                    <label for="surname">Surname</label>
                    <asp:TextBox ID="txt_surname" CssClass ="main" placeholder="Surname" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator id="RequiredFieldValidator1" 
                        ErrorMessage="Surname is required" 
                        ControlToValidate="txt_surname" 
                        Text="Surname is required" 
                        ForeColor="Red" 
                        ValidationGroup="Group1" 
                        runat="server" />
                </div>
                <!--Mail Telefon-->
                <div class="field half">
                    <label for="telefon">Mobile Phone</label>
                    <asp:TextBox ID="txt_telefon" CssClass="main" placeholder ="Exp:05XX-XXX-XX-XX" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator id="required2" 
                        ErrorMessage="Phone is required" 
                        ControlToValidate="txt_telefon" 
                        Text="Phone is required" 
                        ForeColor="Red" 
                        style ="text-align:start;" 
                        ValidationGroup="Group1" 
                        runat="server" />
                    <label for="email">E-Mail</label>
                    <asp:TextBox ID="txt_email" CssClass="main" placeholder ="E-Mail" TextMode="Email" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator id="RequiredFieldValidator2" 
                        ErrorMessage="E-Mail is required" 
                        ControlToValidate="txt_email" 
                        Text="E-Mail is required" 
                        ForeColor="Red" 
                        style ="text-align:start;" 
                        ValidationGroup="Group1" 
                        runat="server" />    
                </div>
                <div class="field">
                    <label for="message">Message</label>
                    <textarea name="message" id="message" rows="6" placeholder="Please fill the all blanks for register."></textarea>
                </div>
                <!--Send-->
                <ul class="actions">
                    <li><asp:Button ID="btn_send" CssClass="button alt" runat="server" Text="Send" OnClick="btn_send_Click" ValidationGroup="Group1"/></li>
                </ul>
                <asp:Label ID="lbl_ip" runat="server" Text=""></asp:Label>
                <div class="copyright">
                    &copy; AYPROFORMA
                </div>
            </div>
        </footer>

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>
    </div>
    </form>
</body>
</html>
