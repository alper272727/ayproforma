﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;


namespace ayproforma
{
    public partial class calculate : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[0].ConnectionString);
        string password, email, company, country, city, district, street, office, no, home_phone, mobile_phone, name, surname, uname, logo;
        string contains_process, contains_cost, process_value;
        int cost_value;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            List<string> keys_process = Request.Form.AllKeys.Where(key_process => key_process.Contains("txt_process")).ToList();
            List<string> keys_cost = Request.Form.AllKeys.Where(key_cost => key_cost.Contains("txt_cost")).ToList();
                     
            foreach (string key_process in keys_process)
            {

                    int index_process = td_process.Controls.OfType<TextBox>().ToList().Count / 2 + 1;
                    int index_cost = td_process.Controls.OfType<TextBox>().ToList().Count / 2 + 1;
                    this.createTextBox_Process("txt_process" + index_process, "txt_cost" + index_cost);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            add_item_panel.Visible = false;

            object users = Session["users"];
            if (users != null)
            {
                before_login_home_page.Visible = false;
                after_login_home_page.Visible = true;
                after_login_panel.Visible = true;
                lbl_login.Text = Session["users"].ToString();
            }
            else
            {
                before_login_home_page.Visible = true;
                after_login_home_page.Visible = false;
                after_login_panel.Visible = false;
            }

            if (!IsPostBack)
            {

                ViewState["Count"] = 0;

                ddlContinents.DataSource = GetData("spGetContinent", null);
                ddlContinents.DataBind();

                System.Web.UI.WebControls.ListItem licontinent = new System.Web.UI.WebControls.ListItem("Select Continent", "-1");
                ddlContinents.Items.Insert(0, licontinent);

                System.Web.UI.WebControls.ListItem licountry = new System.Web.UI.WebControls.ListItem("Select Country", "-1");
                ddlCountries.Items.Insert(0, licountry);

                System.Web.UI.WebControls.ListItem licity = new System.Web.UI.WebControls.ListItem("Select City", "-1");
                ddlCities.Items.Insert(0, licity);

                System.Web.UI.WebControls.ListItem liport = new System.Web.UI.WebControls.ListItem("Select Port", "-1");
                ddlPort.Items.Insert(0, liport);

                ddloperation.DataSource = GetData("spGetTypeOfOperation", null);
                ddloperation.DataBind();

                System.Web.UI.WebControls.ListItem litypeofoperation = new System.Web.UI.WebControls.ListItem("Select Type of Operation", "-1");
                ddloperation.Items.Insert(0, litypeofoperation);

                System.Web.UI.WebControls.ListItem licargotype = new System.Web.UI.WebControls.ListItem("Select Cargo Type", "-1");
                ddlcargotype.Items.Insert(0, licargotype);

                ddlCountries.Enabled = false;
                ddlCities.Enabled = false;
                ddlPort.Enabled = false;
                ddloperation.Enabled = false;
                ddlcargotype.Enabled = false;

                
            }

        }
        protected void lbl_login_Click(object sender, EventArgs e)
        {

        }
        protected void btn_log_out_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("home_page.aspx");
        }

        private DataSet GetData(string SPName, SqlParameter SPParameter)
        {
            string CS = ConfigurationManager.ConnectionStrings[0].ConnectionString;
            SqlConnection con = new SqlConnection(CS);
            SqlDataAdapter da = new SqlDataAdapter(SPName, con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            if (SPParameter != null)
            {
                da.SelectCommand.Parameters.Add(SPParameter);
            }
            DataSet DS = new DataSet();
            da.Fill(DS);
            return DS;
        }

        protected void ddlContinents_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlContinents.SelectedIndex == 0)
            {
                ddlCountries.SelectedIndex = 0;
                ddlCountries.Enabled = false;
                ddlCities.SelectedIndex = 0;
                ddlCities.Enabled = false;
                ddlPort.SelectedIndex = 0;
                ddlPort.Enabled = false;
                ddloperation.SelectedIndex = 0;
                ddloperation.Enabled = false;
                ddlcargotype.SelectedIndex = 0;
                ddlcargotype.Enabled = false;
            }
            else
            {
                ddlCountries.Enabled = true;
                SqlParameter parameter_continent = new SqlParameter("@continent_id", ddlContinents.SelectedValue);
                DataSet ds = GetData("spGetCountriesByContinentId", parameter_continent);

                ddlCountries.DataSource = ds;
                ddlCountries.DataBind();

                System.Web.UI.WebControls.ListItem licountry = new System.Web.UI.WebControls.ListItem("Select Country", "-1");
                ddlCountries.Items.Insert(0, licountry);

                ddlCities.SelectedIndex = 0;
                ddlCities.Enabled = false;
                ddlPort.SelectedIndex = 0;
                ddlPort.Enabled = false;
                ddloperation.SelectedIndex = 0;
                ddloperation.Enabled = false;
                ddlcargotype.SelectedIndex = 0;
                ddlcargotype.Enabled = false;
            }

        }

        protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCountries.SelectedIndex == 0)
            {
                ddlCountries.SelectedIndex = 0;
                ddlCountries.Enabled = false;
                ddlCities.SelectedIndex = 0;
                ddlCities.Enabled = false;
                ddlPort.SelectedIndex = 0;
                ddlPort.Enabled = false;
                ddloperation.SelectedIndex = 0;
                ddloperation.Enabled = false;
                ddlcargotype.SelectedIndex = 0;
                ddlcargotype.Enabled = false;
            }
            else
            {
                ddlCities.Enabled = true;
                SqlParameter parameter_country = new SqlParameter("@Country_Id", ddlCountries.SelectedValue);
                DataSet ds = GetData("spGetCitiesByCountryId", parameter_country);

                ddlCities.DataSource = ds;
                ddlCities.DataBind();

                System.Web.UI.WebControls.ListItem licity = new System.Web.UI.WebControls.ListItem("Select City", "-1");
                ddlCities.Items.Insert(0, licity);

                ddlPort.SelectedIndex = 0;
                ddlPort.Enabled = false;
                ddloperation.SelectedIndex = 0;
                ddloperation.Enabled = false;
                ddlcargotype.SelectedIndex = 0;
                ddlcargotype.Enabled = false;
            }

        }

        protected void ddlCities_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCities.SelectedIndex == 0)
            {
                ddlCities.SelectedIndex = 0;
                ddlCities.Enabled = false;
                ddlPort.SelectedIndex = 0;
                ddlPort.Enabled = false;
                ddloperation.SelectedIndex = 0;
                ddloperation.Enabled = false;
                ddlcargotype.SelectedIndex = 0;
                ddlcargotype.Enabled = false;
            }
            else
            {
                ddlPort.Enabled = true;
                ddloperation.Enabled = true;
                SqlParameter parameter_city = new SqlParameter("@City_Id", ddlCities.SelectedValue);
                DataSet ds = GetData("spGetPortByCityId", parameter_city);

                ddlPort.DataSource = ds;
                ddlPort.DataBind();

                System.Web.UI.WebControls.ListItem liport = new System.Web.UI.WebControls.ListItem("Select Port", "-1");
                ddlPort.Items.Insert(0, liport);
            }
        }

        protected void ddloperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddloperation.SelectedIndex == 0)
            {
                ddloperation.SelectedIndex = 0;
                ddloperation.Enabled = false;
                ddlcargotype.SelectedIndex = 0;
                ddlcargotype.Enabled = false;
            }
            else
            {
                ddlcargotype.Enabled = true;
                SqlParameter parameter_cargo_type = new SqlParameter("@Type_Of_Operation_Id", ddloperation.SelectedValue);
                DataSet ds = GetData("spGetCargoTypeByTypeOfOperation", parameter_cargo_type);

                ddlcargotype.DataSource = ds;
                ddlcargotype.DataBind();

                System.Web.UI.WebControls.ListItem licargo = new System.Web.UI.WebControls.ListItem("Select Cargo Type", "-1");
                ddlcargotype.Items.Insert(0, licargo);

            }
        }

        protected void btn_calculate_Click(object sender, EventArgs e)
        {
            string sorgu = "SELECT * FROM [PDA_CALCULATION].[dbo].[users] where User_Name= @kullanici_adi";

            SqlCommand cmd = new SqlCommand(sorgu, con);

            try
            {
                string user_name = Session["users"].ToString();
                cmd.Parameters.AddWithValue("@kullanici_adi", user_name);
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                SqlDataReader dr = cmd.ExecuteReader();
                password = ds.Tables[0].Rows[0]["Password"].ToString();
                email = ds.Tables[0].Rows[0]["EMail"].ToString();
                company = ds.Tables[0].Rows[0]["Company"].ToString();
                country = ds.Tables[0].Rows[0]["Country"].ToString();
                city = ds.Tables[0].Rows[0]["City"].ToString();
                district = ds.Tables[0].Rows[0]["District"].ToString();
                street = ds.Tables[0].Rows[0]["Street"].ToString();
                office = ds.Tables[0].Rows[0]["Office"].ToString();
                no = ds.Tables[0].Rows[0]["No"].ToString();
                home_phone = ds.Tables[0].Rows[0]["Home_Phone"].ToString();
                mobile_phone = ds.Tables[0].Rows[0]["Mobile_Phone"].ToString();
                name = ds.Tables[0].Rows[0]["Name"].ToString();
                surname = ds.Tables[0].Rows[0]["Surname"].ToString();
                uname = ds.Tables[0].Rows[0]["User_Name"].ToString();
                logo = ds.Tables[0].Rows[0]["Image_Path"].ToString();
                con.Close();

            }
            catch (Exception)
            {
                
                throw;
            }

            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);



            string companyName = company;
            string load_discharge = txt_dad_weight.Text;
            string gross_tonnage = txt_gross_ton.Text;
            string net_tonnage = txt_net_ton.Text;
            string dad_weight = txt_dad_weight.Text;
            string port = ddlPort.SelectedItem.Text.ToString();
            string cargo = txt_cargo_tonaj.Text;
            string cargotype = ddlcargotype.SelectedItem.Text.ToString();
            string operation = ddloperation.SelectedItem.Text.ToString();
            
            int orderNo = 1;

            string imagepath = Server.MapPath(logo);



            iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance(imagepath);




            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[5] {
                            new DataColumn("No", typeof(int)),
                            new DataColumn("Port Costs", typeof(string)),
                            new DataColumn("Euro", typeof(int)),
                            new DataColumn("USD", typeof(int)),
                            new DataColumn("Total", typeof(int))});
            dt.Rows.Add(1, "Harbour Dues", 1, 1, 1);
            dt.Rows.Add(2, "Sanitary Dues", 1, 1, 1);
            dt.Rows.Add(3, "Light Dues", 1, 1, 1);
            dt.Rows.Add(4, "Pilotage Dues", 1, 1, 1);
            dt.Rows.Add(5, "Towage Dues", 1, 1, 1);
            dt.Rows.Add(6, "Mooring Dues", 1, 1, 1);
            dt.Rows.Add(7, "Wharfage Dues", 1, 1, 1);
            dt.Rows.Add(8, "Garbage Dues (Compulsary", 1, 1, 1);
            dt.Rows.Add(9, "Motor Launch Hire", 1, 1, 1);
            dt.Rows.Add(10, "Custom Charges", 1, 1, 1);
            dt.Rows.Add(11, "Chamber of Maritime Dues", 1, 1, 1);
            dt.Rows.Add(12, "Contribution to the Chamber of Shipping", 12, 1, 1);
            dt.Rows.Add(13, "Transportation", 1, 1, 1);
            dt.Rows.Add(14, "Communication", 1, 1, 1);
            dt.Rows.Add(15, "Petties, Postage, Fiscal Stamps, Photocopies", 1, 1, 1);
            dt.Rows.Add(16, "Customer Gratuities", 1, 1, 1);
            dt.Rows.Add(17, "Agency Fees (1-5 Days)", 1, 1, 1);
            dt.Rows.Add(18, "Additional Agency Fees +3 Days", 1, 1, 1);
            dt.Rows.Add(19, "Attendance (Supervision) fee", 1, 1, 1);
            dt.Rows.Add(20, "Bank Charges", 1, 1, 1);
            dt.Rows.Add(21, "Shore Pass Expenses", 1, 1, 1);
            dt.Rows.Add(22, "Anchorage Fees +3 Days", 1, 1, 1);
            dt.Rows.Add(23, "Survey Expenses", 1, 1, 1);
            dt.Rows.Add(24, "Turkish Freight Tax", 1, 1, 1);
            dt.Rows.Add(25, "Shore Pass Expenses", 1, 1, 1);

            int i = 26;
            
            foreach (TextBox textbox in td_process.Controls.OfType<TextBox>())
            {
              

                contains_process = textbox.ID;
                contains_cost = textbox.ID;

                bool process = contains_process.Contains("process");
                bool cost = contains_cost.Contains("cost");

                if (process == true)
                {
                    process_value = textbox.Text;
                }

                if (cost == true)
                {
                  
                    cost_value = Convert.ToInt32(textbox.Text);
                }

                if (cost == true && process == false)
                {
                     dt.Rows.Add(i, process_value, 1, 1, cost_value);
                     i++;
                }

            }
            
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    StringBuilder sb = new StringBuilder();

                    //Generate Invoice (Bill) Header.
                    sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");
                    sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b>Order Sheet</b></td></tr>");
                    sb.Append("<tr><td colspan = '2'></td></tr>");
                    sb.Append("<tr><td><b>Order No: </b>");
                    sb.Append(orderNo);
                    sb.Append("</td><td align = 'right'><b>Date: </b>");
                    sb.Append(DateTime.Now);
                    sb.Append(" </td></tr>");
                    sb.Append(" </td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Company Name: </b>");
                    sb.Append(companyName);
                    sb.Append("</td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Port Name: </b>");
                    sb.Append(port);
                    sb.Append("</td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Ship's Name: </b>");
                    sb.Append(txt_ship_name.Text);
                    sb.Append("</td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Flag: </b>");
                    sb.Append("Turkey");
                    sb.Append(" </td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Type of Operation: </b>");
                    sb.Append(operation);
                    sb.Append("</td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Cargo Type: </b>");
                    sb.Append(cargotype);
                    sb.Append(" </td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Cargo: </b>");
                    sb.Append(cargo);
                    sb.Append(" </td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Load/Discharge (mt): </b>");
                    sb.Append(load_discharge);
                    sb.Append(" </td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Gross Tonnage: </b>");
                    sb.Append(gross_tonnage);
                    sb.Append(" </td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Net Tonnage: </b>");
                    sb.Append(net_tonnage);
                    sb.Append(" </td></tr>");
                    sb.Append("<tr><td colspan = '2'><b>Dad Weight: </b>");
                    sb.Append(dad_weight);
                    sb.Append(" </td></tr>");
                    


                   

                   foreach (TextBox textbox in td_process.Controls.OfType<TextBox>())
                    {


                        contains_process = textbox.ID;
                        contains_cost = textbox.ID;

                        bool process = contains_process.Contains("process");
                        bool cost = contains_cost.Contains("cost");

                        if (process == true)
                        {
                            process_value = textbox.Text;
                        }

                        if (cost == true)
                        {
                            cost_value = Convert.ToInt32(textbox.Text);
                        }

                        if (cost == true && process == false)
                        {
                            sb.Append("<tr><td colspan = '2'><b>" + process_value + "</b>");
                            sb.Append(cost_value);
                            sb.Append(" </td></tr>");
                        }

                       

                    }
                   sb.Append("</table>");
                   sb.Append("<br />");

                    //Generate Invoice (Bill) Items Grid.
                    sb.Append("<table border = '1'>");
                    sb.Append("<tr>");
                    foreach (DataColumn column in dt.Columns)
                    {
                        sb.Append("<th style = 'background-color: #D20B0C;color:#ffffff'>");
                        sb.Append(column.ColumnName);
                        sb.Append("</th>");
                    }
                    sb.Append("</tr>");
                    foreach (DataRow row in dt.Rows)
                    {
                        sb.Append("<tr>");
                        foreach (DataColumn column in dt.Columns)
                        {
                            sb.Append("<td>");
                            sb.Append(row[column]);
                            sb.Append("</td>");
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("<tr><td align = 'right' colspan = '");
                    sb.Append(dt.Columns.Count - 1);
                    sb.Append("'>Total</td>");
                    sb.Append("<td>");
                    sb.Append(dt.Compute("sum(Total)", ""));
                    sb.Append("</td>");
                    sb.Append("</tr></table>");

                    //Export HTML String as PDF.
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=" + company + "-" + orderNo + ".pdf");
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);

 
                    StringReader sr = new StringReader(sb.ToString());
                    Document pdfDoc = new Document(PageSize.A4, 20f, 20f, 20f, 20f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                   

                    gif.ScaleToFit(100f, 300f);

                    //Give space before image

                    gif.SpacingBefore = 10f;

                    //Give some space after the image

                    gif.SpacingAfter = 1f;
                    gif.Alignment = Element.ALIGN_RIGHT;
                    gif.Alignment = Element.ALIGN_JUSTIFIED_ALL;
   

                    pdfDoc.Add(gif);

                    htmlparser.Parse(sr);

                    pdfDoc.Close();

                    Response.Write(pdfDoc);
                    Response.End();
                }

            }

        }


        protected void add_item_ok_Click(object sender, EventArgs e)
        {
            add_item_panel.Visible = false;
        }

        protected void add_item_cancel_Click(object sender, EventArgs e)
        {
            add_item_panel.Visible = false;
        }

        protected void add_item_Click(object sender, EventArgs e)
        {

            int index_process = td_process.Controls.OfType<TextBox>().ToList().Count / 2 + 1;
            int index_cost = td_process.Controls.OfType<TextBox>().ToList().Count / 2 + 1;
            this.createTextBox_Process("txt_process" + index_process, "txt_cost" + index_cost);
            

        }

        private void createTextBox_Process(string process, string cost)
        {     
                add_item_panel.Visible = true;

                int index_process = td_process.Controls.OfType<TextBox>().ToList().Count / 2 + 1;

                TextBox txtbox = new TextBox();
                txtbox.ID = process;
                
                Label lbl = new Label();
                lbl.ID = "lbl" + process + index_process;
                lbl.Text = "Add Process" + index_process;
                lbl.BorderColor = System.Drawing.Color.White;
                lbl.Font.Bold = true;
                lbl.ForeColor = System.Drawing.Color.White;

                td_process.Controls.Add(lbl);
                td_process.Controls.Add(txtbox);

                TextBox txtbox2 = new TextBox();
                txtbox2.ID = cost;

                Label lbl2 = new Label();
                lbl2.ID = "lbl2_" + cost + index_process;
                lbl2.Text = "Add Cost" + index_process;
                lbl2.BorderColor = System.Drawing.Color.White;
                lbl2.Font.Bold = true;
                lbl2.ForeColor = System.Drawing.Color.White;

                txtbox2.Attributes.Add("runat", "server");
                /*txtbox2.TextMode = TextBoxMode.Number;*/

                td_process.Controls.Add(lbl2);
                td_process.Controls.Add(txtbox2);
            }
    }
}