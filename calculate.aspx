﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="calculate.aspx.cs" Inherits="ayproforma.calculate" MaintainScrollPositionOnPostback="true"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDA Calculate</title>
    <link href="JS/js-image-slider.css" rel="stylesheet" type="text/css" />
    <script src="JS/js-image-slider.js" type="text/javascript"></script>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <script src="https://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.css"
        rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
    <link rel="stylesheet" href="/resources/demos/style.css"/>
    <script type="text/javascript">
        $(function () {
            $("#txt_start").datepicker();
        });

        $(function () {
            $("#txt_end").datepicker();
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <!-- Header -->

            <header id="header">
                <div class="inner">
                    <a href="home_page.aspx" class="logo"><strong>PDA Calculator</strong> by AYPROFORMA</a>
                    <asp:Panel ID="after_login_panel" runat="server">
                        Welcome
                        <asp:LinkButton ID="lbl_login" CssClass="main" OnClick="lbl_login_Click" Font-Bold="true" ForeColor="#6cc091" runat="server"></asp:LinkButton>
                    </asp:Panel>
                    <nav id="nav">
                        <asp:Panel ID="before_login_home_page" runat="server">
                            <a href="home_page.aspx">Home Page</a>
                            <a href="login.aspx">Log in</a>
                            <a href="login.aspx">Our Mission</a>
                            <a href="login.aspx">Our Vision</a>
                            <a href="login.aspx">Who We Are?</a>
                        </asp:Panel>
                        <asp:Panel ID="after_login_home_page" runat="server">
                            <a href="home_page.aspx">Home Page</a>
                            <a href="home_page.aspx">My Birds</a>
                            <a href="settings.aspx">Settings</a>
                            <asp:LinkButton ID="btn_log_out" OnClick="btn_log_out_Click" runat="server">Log out</asp:LinkButton>
                        </asp:Panel>

                    </nav>
                    <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
                </div>
            </header>




            <!-- Banner -->
            <section id="banner">
                <div class="inner">
                    <header>
                        <h1>Welcome to PDA Calculator</h1>
                    </header>
                    <table>
                        <!--Continent -Country - Cities - Port-->
                        <tr>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label1" runat="server" Text="Continent" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label>
                                <asp:DropDownList ID="ddlContinents" runat="server"
                                DataTextField="Continent" DataValueField="Continent_Id"
                                OnSelectedIndexChanged="ddlContinents_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList></td>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label2" runat="server" Text="Country" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label><asp:DropDownList ID="ddlCountries"
                                DataTextField="Country" DataValueField="Country_Id"
                                OnSelectedIndexChanged="ddlCountries_SelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                            </asp:DropDownList></td>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label3" runat="server" Text="City" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label><asp:DropDownList ID="ddlCities"
                                DataTextField="City" DataValueField="City_Id" OnSelectedIndexChanged="ddlCities_SelectedIndexChanged"
                                runat="server" AutoPostBack="true">
                            </asp:DropDownList></td>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label4" runat="server" Text="Port" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label><asp:DropDownList ID="ddlPort"
                                DataTextField="Port_Name" DataValueField="Port_Id" runat="server">
                            </asp:DropDownList></td>
                        </tr>
                        </table>
                        <!--Type of Operation - Cargo Type - Cargo Tonnage - Flag -->
                        <table>
                         <tr>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label5" runat="server" Text="Type of Operation" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label><asp:DropDownList ID="ddloperation" runat="server"
                                DataTextField="Type_Of_Operation" DataValueField="Type_Of_Operation_Id"
                                AutoPostBack="true" OnSelectedIndexChanged="ddloperation_SelectedIndexChanged"></asp:DropDownList></td>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label6" runat="server" Text="Cargo Type" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label><asp:DropDownList ID="ddlcargotype" runat="server"
                                DataTextField="Cargo_Type" DataValueField="Cargo_Type_Id" AutoPostBack="true"></asp:DropDownList></td>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label7" runat="server" Text="Cargo Tonnage" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label><asp:TextBox ID="txt_cargo_tonaj" placeholder="Cargo tonnage Exp: 10000" runat="server"></asp:TextBox></td>
                            <td>&nbsp&nbsp&nbsp<asp:RadioButtonList ID="radio_flag" 
                                RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server">
                                <asp:ListItem Text="Turkish Flag" Value="turkish" />
                                <asp:ListItem Text="Abroad Flag" Value="abroad" />
                                </asp:RadioButtonList></td>
                        </tr>
                        </table>
                        <!--Ship's Name - Gross Tonnage - Net Tonnage - Dad Weight-->
                        <table>
                        <tr>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label8" runat="server" Text="Ship's Name" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label>
                                <asp:TextBox ID="txt_ship_name" placeholder="Ship Name Exp: Bandirma" runat="server"></asp:TextBox></td>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label9" runat="server" Text="Gross Tonnage" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label>
                                <asp:TextBox ID="txt_gross_ton" placeholder="Gross Tonnage of Ship Exp: 20000" runat="server"></asp:TextBox></td>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label10" runat="server" Text="Net Tonnage" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label>
                                <asp:TextBox ID="txt_net_ton"   placeholder="Net Tonnage of Ship Exp: 15000" runat="server"></asp:TextBox></td>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label11" runat="server" Text="Dad Weight" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label>
                                <asp:TextBox ID="txt_dad_weight" placeholder="Dad weight of Ship Exp: 5000" runat="server"></asp:TextBox></td>
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="Label12" runat="server" Text="Add Process" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label>
                                &nbsp&nbsp&nbsp<asp:LinkButton ID="add_item" OnClick="add_item_Click" CssClass="icon fas fa-plus"
                                        runat="server" Width="300px"
                                    Style="display: inline; text-align: center;"></asp:LinkButton></td>
                          </tr>
                            
                            <asp:Panel ID="add_item_panel"  runat="server">
                                <tr>
                                        <td colspan="1" id="td_process" runat="server">&nbsp&nbsp&nbsp<asp:Label ID="Label13" runat="server" Text="" BorderColor="White" Font-Bold="true"
                                            ForeColor="White" Style="display: inline; text-align: center;">
                                            </asp:Label>&nbsp&nbsp&nbsp
                                            <asp:Label ID="Label14" runat="server" Text="" BorderColor="White" Font-Bold="true"
                                            ForeColor="White" Style="display: inline; text-align: center;"></asp:Label> 
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="add_item_cancel" CssClass="icon fas fa-times" Font-Bold="true" ForeColor="red" runat="server"
                                                OnClick="add_item_cancel_Click" Style="vertical-align: middle">
                                            </asp:LinkButton>
                                        </td>
                                </tr>
                                    </asp:Panel>
                         <tr>
                                         
                            <td>&nbsp&nbsp&nbsp<asp:Label ID="lbl_start" runat="server" Text="Start Date" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label>
                            &nbsp&nbsp&nbsp<asp:TextBox ID="txt_start" placeholder="Start Date" runat="server" onKeyPress="javascript:datepicker();"></asp:TextBox></td>
                             <td>&nbsp&nbsp&nbsp<asp:Label ID="lbl_end" runat="server" Text="End Date" BorderColor="White" Font-Bold="true" 
                                ForeColor="White" Style="display: inline; text-align: center;"></asp:Label>
                            &nbsp&nbsp&nbsp<asp:TextBox ID="txt_end" runat="server" placeholder="End Date" onKeyPress="javascript:datepicker();"></asp:TextBox></td>
                        </tr>

                    </table>
                    <footer>
                        <asp:Button ID="btn_calculate" CssClass="button" OnClick="btn_calculate_Click" runat="server" Text="Calculate" /> 
                    </footer>
                </div>
            </section>
        </div>
    </form>
</body>
</html>
