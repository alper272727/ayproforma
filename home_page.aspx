﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="home_page.aspx.cs" Inherits="ayproforma.home_page" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDA Calculate</title>
    <link href="JS/js-image-slider.css" rel="stylesheet" type="text/css"/>
    <script src="JS/js-image-slider.js" type="text/javascript"></script>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="assets/css/main.css" />
    <script src="https://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function success_send_contact() {
            swal({
                title: "Contact Form!",
                text: "Contact form sending",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            },
function () {
    setTimeout(function () {
        swal("Contact Form sent successfully!");
    }, 2000);
});
        }
        function alert_send_contact() {
            swal({
                title: 'Contact Form!',
                text: 'Oops! Contact Info was not send.',
                type: 'error'
            });
        }

        function log_in() {
            swal({
                title: 'Please, Log in!',
                text: 'Oops! Please, Firstly log in.',
                type: 'info'
            }, function () {
                window.location = "login.aspx";
            }, 1000);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <!-- Header -->

            <header id="header">
                <div class="inner">
                    <a href="home_page.aspx" class="logo"><strong>PDA Calculator</strong> by AYPROFORMA</a>
                    <asp:Panel ID="after_login_panel" runat="server">
                        Welcome
                        <asp:LinkButton ID="lbl_login" CssClass="main" OnClick="lbl_login_Click" Font-Bold="true" ForeColor="#6cc091" runat="server"></asp:LinkButton>
                    </asp:Panel>
                    <nav id="nav">
                        <asp:Panel ID="before_login_home_page" runat="server">
                            <a href="home_page.aspx">Home Page</a>
                            <a href="login.aspx">Log in</a>
                            <a href="login.aspx">Our Mission</a>
                            <a href="login.aspx">Our Vision</a>
                            <a href="login.aspx">Who We Are?</a>
                        </asp:Panel>
                        <asp:Panel ID="after_login_home_page" runat="server">
                            <a href="home_page.aspx">Home Page</a>
                            <a href="home_page.aspx">My Birds</a>
                            <a href="settings.aspx">Settings</a>
                            <asp:LinkButton ID="btn_log_out" OnClick="btn_log_out_Click" runat="server">Log out</asp:LinkButton>
                        </asp:Panel>

                    </nav>
                    <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
                </div>
            </header>




            <!-- Banner -->
            <section id="banner">
                <div class="inner">
                    <header>
                        <h1>Welcome to PDA Calculator</h1>
                    </header>

                    <div class="flex ">

                        <div>
                            <span class="icon fa-list-ul"></span>
                            <h3>Datas</h3>
                            <p>Entry all datas</p>
                        </div>

                        <div>
                            <span class="icon fa-calculator"></span>
                            <h3>Calculate</h3>
                            <p>Run the robot for calculator</p>
                        </div>

                        <div>
                            <span class="icon fa-check-circle"></span>
                            <h3>Result</h3>
                            <p>Ready your bid</p>
                        </div>
                    </div>
                    <footer>
                        <asp:Button ID="btn_calculate" OnClick="btn_calculate_Click" CssClass="button" runat="server" Text="Calculate" />
                    </footer>
                </div>
            </section>

            <!-- Three -->
            <section id="three" class="wrapper align-center">
                <div class="inner">
                    <div class="flex flex-2">
                        <article>
                            <div class="image round">
                                <img src="images/gemi.jpg" alt="Pic 01" />
                            </div>
                            <header>
                                <h3>Gemicilik Teknolojisi<br />
                                    hızlı, kolay ve anlaşılır</h3>
                            </header>
                            <p>Akıllı denizcilik felsefesi ile teknolojiyi<br />
                                kullanarak gemi sahipleri/operatörler acentalar arasındaki<br />
                                boşlukları kapatarak; hızlı, kolay ve zamandan tasarruf ediyoruz.</p>
                            <footer>
                                <a href="#" class="button">Learn More</a>
                            </footer>
                        </article>
                        <article>
                            <div class="image round">
                                <img src="images/hesaplama.jpg" alt="Pic 02" />
                            </div>
                            <header>
                                <h3>Hesaplama Robotu ile<br />
                                    Teklif Verin </h3>
                            </header>
                            <p>Hesaplama robotu ile müşterilenize<br />
                                hızlı, kolay ve güvenilir teklifler verebilirsiniz<br />
                                siz sadece gemi bilgilerini yazın, bırakın robot hesaplasın.</p>
                            <footer>
                                <a href="#" class="button">Learn More</a>
                            </footer>
                        </article>
                    </div>
                </div>
            </section>

            <!-- Footer -->
            <footer id="footer">
                <div class="inner">
                    <h3>Contact with Us</h3>
                    <div class="field half first">
                        <!--Ad-Soyad-->
                        <label for="name">Name</label>
                        <asp:TextBox ID="txt_name" CssClass="main" placeholder="Name" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="required1" ErrorMessage="Name is required" 
                         ControlToValidate="txt_name" Text="Name is required" 
                         ValidationGroup="Group1" ForeColor="Red" runat="server" />
                        <label for="surname">Surname</label>
                        <asp:TextBox ID="txt_surname" CssClass="main" placeholder="Surname" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Surname is required" 
                        ControlToValidate="txt_surname" Text="Surname is required" 
                        ValidationGroup="Group1" ForeColor="Red" runat="server" />
                    </div>
                    <!--Mail Telefon-->
                    <div class="field half">
                        <label for="telefon">Mobile Phone</label>
                        <asp:TextBox ID="txt_telefon" CssClass="main" placeholder="Exp:05XX-XXX-XX-XX" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="required2" ErrorMessage="Phone is required" 
                        ValidationGroup="Group1" ControlToValidate="txt_telefon" Text="Phone is required" ForeColor="Red" Style="text-align: start;" runat="server" />
                        <label for="email">E-Mail</label>
                        <asp:TextBox ID="txt_email" CssClass="main" placeholder="E-Mail" TextMode="Email" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="E-Mail is required" 
                        ValidationGroup="Group1" ControlToValidate="txt_email" Text="E-Mail is required" ForeColor="Red" Style="text-align: start;" runat="server" />
                    </div>
                    <div class="field">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" rows="6" placeholder="Please fill the all blanks for register."></textarea>
                    </div>
                    <!--Send-->
                    <ul class="actions">
                        <li>
                            <asp:Button ID="btn_send" CssClass="button alt" runat="server" Text="Send" 
                                ValidationGroup="Group1" OnClick="btn_send_Click" /></li>
                    </ul>
                    <asp:Label ID="lbl_ip" runat="server" Text=""></asp:Label>
                    <div class="copyright">
                        &copy; AYPROFORMA
                    </div>
                </div>
            </footer>

            <!-- Scripts -->
            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>
        </div>
    </form>
</body>
</html>
