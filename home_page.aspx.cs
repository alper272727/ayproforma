﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;


namespace ayproforma
{
    public partial class home_page : System.Web.UI.Page
    {
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[0].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            
            object users = Session["users"];
            if (users != null)
            {
                before_login_home_page.Visible = false;
                after_login_home_page.Visible = true;
                after_login_panel.Visible = true;
                lbl_login.Text = Session["users"].ToString();
            }
            else
            {
                before_login_home_page.Visible = true;
                after_login_home_page.Visible = false;
                after_login_panel.Visible = false;
            }
        }

        protected void lbl_login_Click(object sender, EventArgs e)
        {

        }

        protected void btn_log_out_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("home_page.aspx");
        }

        protected void btn_send_Click(object sender, EventArgs e)
        {
            GetIP();

            string body =  @"<tr><td>" + "Ad      :" +     "<b>" + txt_name.Text    + "</b>"+"</td></tr>" +
                             "<tr><td>" + "Soyad   :" + "<b>" + txt_surname.Text + "</b>" + "</td></tr>" +
                             "<tr><td>" + "Telefon :" + "<b>" + txt_telefon.Text + "</b>" + "</td></tr>" +
                             "<tr><td>" + "E-Mail  :" + "<b>" + txt_email.Text + "</b>" + "</td></tr>" +
                             "<tr><td>" + "Mesaj   :" + "<b>" + Request.Form["message"] + "</b>" + "</td></tr>" +
                             "<tr><td>" + "IP Address   :" + "<b>" + GetIP().ToString() + "</b>" + "</td></tr>"; 
    
            
            string subject = "Yeni Bir Müşteri Adayı Var!";

            try
            {
                string email = "ylmzalper9427@gmail.com";
                MailMessage message = new MailMessage(email, email, subject, body);
                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("ylmzalper9427@gmail.com", "Alper272727.");
                client.Send(message);
                

                /*Gönderilen mesajları tabloda kayıt altına almak için*/
                string sorgu_kontakt = "INSERT INTO [PDA_CALCULATION].[dbo].[contact] (name, surname, email, phone, message) Values (@name, @surname, @email, @phone, @message)";

                SqlCommand cmd_contact = new SqlCommand(sorgu_kontakt, connection);
                connection.Open();

                try
                {
                    cmd_contact.Parameters.AddWithValue("@name", txt_name.Text);
                    cmd_contact.Parameters.AddWithValue("@surname", txt_surname.Text);
                    cmd_contact.Parameters.AddWithValue("@email", txt_email.Text);
                    cmd_contact.Parameters.AddWithValue("@phone", txt_telefon.Text);
                    cmd_contact.Parameters.AddWithValue("@message", Request.Form["message"]);

                    cmd_contact.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_send_contact();", true);
                    connection.Close();
                    txt_name.Text = "";
                    txt_surname.Text = "";
                    txt_email.Text = "";
                    txt_telefon.Text = "";
                    Request.Form["message"] = "";
                    

                }
                catch (Exception) { }

                

                
            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_send_contact();", true);
            }
            /*Kayıt altına alma işlemi bittiği yer*/

        }

        public static String GetIP()
        {
            String ip =
                HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            return ip;
        }

        protected void btn_calculate_Click(object sender, EventArgs e)
        {
            object users = Session["users"];
            if (users != null)
            {
                before_login_home_page.Visible = false;
                after_login_home_page.Visible = true;
                after_login_panel.Visible = true;
                Response.Redirect("calculate.aspx");
            }
            else
            {
                before_login_home_page.Visible = true;
                after_login_home_page.Visible = false;
                after_login_panel.Visible = false;
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "log_in();", true);
            }
        }

        }


    }