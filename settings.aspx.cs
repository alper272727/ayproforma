﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Data;
using System.Drawing;

namespace ayproforma
{
    public partial class settings : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[0].ConnectionString);
        string password, email, company, country, city, district, street, office, no, home_phone, mobile_phone, name, surname, uname, logo;

        static String activitioncode;


        protected void Page_Load(object sender, EventArgs e)
        {
    
                
 
                /*Kullanıcı Adı*/

                /*Şifre*/
                before_password_edit.Visible = true;
                after_password_edit.Visible = false;
                /*İsim-Soyisim*/
                before_name_surname_edit.Visible = true;
                after_name_surname_edit.Visible = false;
                /*Email*/
                before_email_edit.Visible = true;
                after_email_edit.Visible = false;
                confirm_after_email_edit.Visible = false;
                /*Home Phone*/
                before_home_phone_edit.Visible = true;
                after_home_phone_edit.Visible = false;
                /*Mobile Phone*/
                before_mobile_phone_edit.Visible = true;
                after_mobile_phone_edit.Visible = false;
                /*Company*/
                before_company_edit.Visible = true;
                after_company_edit.Visible = false;
                /*Country*/
                before_country_edit.Visible = true;
                after_country_edit.Visible = false;
                /*City*/
                before_city_edit.Visible = true;
                after_city_edit.Visible = false;
                /*District*/
                before_district_edit.Visible = true;
                after_district_edit.Visible = false;
                /*Street*/
                before_street_edit.Visible = true;
                after_street_edit.Visible = false;
                /*Street*/
                before_office_edit.Visible = true;
                after_office_edit.Visible = false;
                /*No*/
                before_no_edit.Visible = true;
                after_no_edit.Visible = false;
                /*Logo*/
                before_logo_edit.Visible = true;
                after_logo_edit.Visible = false;

                string sorgu = "SELECT * FROM [PDA_CALCULATION].[dbo].[users] where User_Name= @kullanici_adi";

                SqlCommand cmd = new SqlCommand(sorgu, con);

                object users = Session["users"];
                if (users != null)
                {
                    after_login_panel.Visible = true;
                    lbl_login.Text = Session["users"].ToString();
                }

                try
                {
                    string user_name = Session["users"].ToString();
                    cmd.Parameters.AddWithValue("@kullanici_adi", user_name);
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    SqlDataReader dr = cmd.ExecuteReader();
                    password = ds.Tables[0].Rows[0]["Password"].ToString();
                    email = ds.Tables[0].Rows[0]["EMail"].ToString();
                    company = ds.Tables[0].Rows[0]["Company"].ToString();
                    country = ds.Tables[0].Rows[0]["Country"].ToString();
                    city = ds.Tables[0].Rows[0]["City"].ToString();
                    district = ds.Tables[0].Rows[0]["District"].ToString();
                    street = ds.Tables[0].Rows[0]["Street"].ToString();
                    office = ds.Tables[0].Rows[0]["Office"].ToString();
                    no = ds.Tables[0].Rows[0]["No"].ToString();
                    home_phone = ds.Tables[0].Rows[0]["Home_Phone"].ToString();
                    mobile_phone = ds.Tables[0].Rows[0]["Mobile_Phone"].ToString();
                    name = ds.Tables[0].Rows[0]["Name"].ToString();
                    surname = ds.Tables[0].Rows[0]["Surname"].ToString();
                    uname = ds.Tables[0].Rows[0]["User_Name"].ToString();
                    logo = ds.Tables[0].Rows[0]["Image_Path"].ToString();
                    con.Close();
                    lbl_user_name.Text = uname;
                    lbl_name_surname.Text = name + " " + surname;
                    lbl_email_edit.Text = email;
                    lbl_home_phone.Text = home_phone;
                    lbl_mobile_phone.Text = mobile_phone;
                    lbl_country.Text = country;
                    lbl_city.Text = city;
                    lbl_district.Text = district;
                    lbl_street.Text = street;
                    lbl_office.Text = office;
                    lbl_no.Text = no;
                    lbl_company.Text = company;
                    old_image.ImageUrl = logo;
                    new_image.ImageUrl = logo;

                }
                catch (Exception)
                {

                }
        }

        protected void lbl_login_Click(object sender, EventArgs e)
        {
            Server.Transfer("settings.aspx", true);
        }

        protected void btn_log_out_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("home_page.aspx");
        }
        protected void btn_send_Click(object sender, EventArgs e)
        {
            GetIP();

            string body = @"<tr><td>" + "Ad      :" + "<b>" + txt_name.Text + "</b>" + "</td></tr>" +
                             "<tr><td>" + "Soyad   :" + "<b>" + txt_surname.Text + "</b>" + "</td></tr>" +
                             "<tr><td>" + "Telefon :" + "<b>" + txt_telefon.Text + "</b>" + "</td></tr>" +
                             "<tr><td>" + "E-Mail  :" + "<b>" + txt_email.Text + "</b>" + "</td></tr>" +
                             "<tr><td>" + "Mesaj   :" + "<b>" + Request.Form["message"] + "</b>" + "</td></tr>" +
                             "<tr><td>" + "IP Address   :" + "<b>" + GetIP().ToString() + "</b>" + "</td></tr>";


            string subject = "Yeni Bir Müşteri Adayı Var!";

            try
            {
                string email = "ylmzalper9427@gmail.com";
                MailMessage message = new MailMessage(email, email, subject, body);
                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("ylmzalper9427@gmail.com", "Alper272727.");
                client.Send(message);
                lbl_send_contact_info.Text = "Contact Info was send succesfully";

                /*Gönderilen mesajları tabloda kayıt altına almak için*/
                string sorgu_kontakt = "INSERT INTO [PDA_CALCULATION].[dbo].[contact] (name, surname, email, phone, message) Values (@name, @surname, @email, @phone, @message)";

                SqlCommand cmd_contact = new SqlCommand(sorgu_kontakt, con);
                con.Open();

                try
                {
                    cmd_contact.Parameters.AddWithValue("@name", txt_name.Text);
                    cmd_contact.Parameters.AddWithValue("@surname", txt_surname.Text);
                    cmd_contact.Parameters.AddWithValue("@email", txt_email.Text);
                    cmd_contact.Parameters.AddWithValue("@phone", txt_telefon.Text);
                    cmd_contact.Parameters.AddWithValue("@message", Request.Form["message"]);

                    cmd_contact.ExecuteNonQuery();
                    con.Close();

                }
                catch (Exception) { }




            }
            catch (Exception)
            {
                lbl_send_contact_info.Text = "Oops! Contact Info was not send.";
            }
            /*Kayıt altına alma işlemi bittiği yer*/

        }

        public static String GetIP()
        {
            String ip =
                HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            return ip;
        }
        /*User Name Edit*/


        /*Password Edit*/
        protected void btn_before_password_edit(object sender, EventArgs e)
        {
            before_password_edit.Visible = false;
            after_password_edit.Visible = true;
            lbl_password_text.Text = "";
        }

        protected void btn_password_edit_ok(object sender, EventArgs e)
        {
            if (password == txt_old_password.Text)
            {
                if (txt_password.Text == txt_password2.Text && txt_password.Text != "")
                {
                    string update_password = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Password ='" + txt_password.Text + "' where Password= @password ";
                    SqlCommand cmd_password = new SqlCommand(update_password, con);
                    con.Open();
                    try
                    {
                        cmd_password.Parameters.AddWithValue("@password", password);
                        cmd_password.ExecuteNonQuery();
                        con.Close();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_password();", true);
                    }
                    catch (Exception)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_password();", true);
                        before_password_edit.Visible = false;
                        after_password_edit.Visible = true;

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_password_wrong();", true);
                    after_password_edit.Visible = true;
                    before_password_edit.Visible = false;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_password_invalid();", true);
                after_password_edit.Visible = true;
                before_password_edit.Visible = false;
            }
            con.Close();
        }

        protected void btn_password_edit_cancel(object sender, EventArgs e)
        {
            before_password_edit.Visible = true;
            after_password_edit.Visible = false;
        }

        /*Name-Surname Edit*/
        protected void btn_before_name_surname_edit(object sender, EventArgs e)
        {
            before_name_surname_edit.Visible = false;
            after_name_surname_edit.Visible = true;

        }
        protected void btn_name_surname_edit_ok(object sender, EventArgs e)
        {
            if (txt_name_edit.Text != "" && txt_surnmame_edit.Text != "")
            {


                string update_name = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Name ='" + txt_name_edit.Text + "' where User_Name = @user_name ";
                string update_surname = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Surname ='" + txt_surnmame_edit.Text + "' where User_Name = @user_name ";
                SqlCommand cmd_name = new SqlCommand(update_name, con);
                SqlCommand cmd_surname = new SqlCommand(update_surname, con);
                con.Open();
                try
                {
                    cmd_name.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_surname.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_name.ExecuteNonQuery();
                    cmd_surname.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_name_surname();", true);
                    lbl_name_surname.Text = txt_name_edit.Text + " " + txt_surnmame_edit.Text;
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_name_surname();", true);
                    before_password_edit.Visible = false;
                    after_password_edit.Visible = true;

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_name_surname();", true);
            }
           
        }

        protected void btn_name_surname_edit_cancel(object sender, EventArgs e)
        {
            before_name_surname_edit.Visible = true;
            after_name_surname_edit.Visible = false;
        }
        /*E-Mail Edit*/
        protected void btn_before_email_edit(object sender, EventArgs e)
        {
            
            before_email_edit.Visible = false;
            after_email_edit.Visible = true;
            txt_email_edit.Text = "";
        }
        protected void btn_email_edit_ok(object sender, EventArgs e)
        {
            before_email_edit.Visible = false;
            after_email_edit.Visible = false;
            confirm_after_email_edit.Visible = true;
                if (txt_email_edit.Text != "")
                {
                    Random random = new Random();
                    activitioncode = random.Next(1001, 9999).ToString();


                    string update_activitioncode = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Activation_Code ='" + activitioncode + "' where User_Name = @user_name ";


                    SqlCommand cmd_activitioncode = new SqlCommand(update_activitioncode, con);
                    con.Open();
                    try
                    {

                        cmd_activitioncode.Parameters.AddWithValue("@user_name", Session["users"].ToString());

                        cmd_activitioncode.ExecuteNonQuery();
                        sendcode();
                    }
                    catch (Exception)
                    {
                        before_password_edit.Visible = false;
                        after_password_edit.Visible = true;

                    }
                    con.Close();

                }
                else if (txt_email_edit.Text == email)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_email();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_invalid_email();", true);
                }
            /*Page_Load(sender, e);*/
           
            
        }
 
        protected void btn_email_edit_cancel(object sender, EventArgs e)
        {   
            before_email_edit.Visible = true;
            after_email_edit.Visible = false;
            confirm_after_email_edit.Visible = false;
        }
        protected void btn_confirm_email_edit_ok(object sender, EventArgs e)
        {

            before_email_edit.Visible = false;
            after_email_edit.Visible = false;
            confirm_after_email_edit.Visible = true;

            String activation_code;
            string sorgu_activation_code = "SELECT Activation_Code FROM [PDA_CALCULATION].[dbo].[users] where User_Name= @kullanici_adi";
            SqlCommand cmd = new SqlCommand(sorgu_activation_code, con);
            string user_name = Session["users"].ToString();
            cmd.Parameters.AddWithValue("@kullanici_adi", user_name);
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataSet ds = new DataSet();
            da.Fill(ds);
            SqlDataReader dr = cmd.ExecuteReader();
            activation_code = ds.Tables[0].Rows[0]["Activation_Code"].ToString();

            if (activation_code == txt_activation_code.Text)
            {
                con.Close();
                changeEmail();
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_email();", true);
                lbl_email_edit.Text = txt_email_edit.Text;
                confirm_after_email_edit.Visible = false;
                after_email_edit.Visible = false;
                before_email_edit.Visible = true;


            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_act_code();", true);  
            }
           
            


        }

        private void changeEmail()
        {
            con.Open();
            string update_email = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Email ='" + txt_email_edit.Text + "' where User_Name = @user_name ";    
            SqlCommand cmd_email = new SqlCommand(update_email, con);

            try
            {
                cmd_email.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                cmd_email.ExecuteNonQuery();
            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_update_email();", true);
                throw;
            }
            con.Close();
            confirm_after_email_edit.Visible = false;

        }
        protected void btn_confirm_email_edit_cancel(object sender, EventArgs e)
        {
            before_email_edit.Visible = true;
            after_email_edit.Visible = false;
            confirm_after_email_edit.Visible = false;
            
        }
        /*Home Phone Edit*/
        protected void btn_before_home_phone_edit(object sender, EventArgs e)
        {
            before_home_phone_edit.Visible = false;
            after_home_phone_edit.Visible = true;
            txt_home_phone.Text = "";
        }
        protected void btn_home_phone_edit_ok(object sender, EventArgs e)
        {
            before_home_phone_edit.Visible = true;
            after_home_phone_edit.Visible = false;

            if (txt_home_phone.Text != "")
            {


                string update_home_phone = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Home_Phone ='" + txt_home_phone.Text + "' where User_Name = @user_name ";
                SqlCommand cmd_home_phone = new SqlCommand(update_home_phone, con);
                con.Open();
                try
                {
                    cmd_home_phone.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_home_phone.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_home_phone();", true);
                    lbl_home_phone.Text = txt_home_phone.Text;
                   
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_home_phone();", true);
                    before_password_edit.Visible = false;
                    after_password_edit.Visible = true;
                   

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_changed_home_phone();", true);
            }
        }

        protected void btn_home_phone_edit_cancel(object sender, EventArgs e)
        {
            before_home_phone_edit.Visible = true;
            after_home_phone_edit.Visible = false;
        }
        /*Mobile Phone Edit*/
        protected void btn_before_mobile_phone_edit(object sender, EventArgs e)
        {
            before_mobile_phone_edit.Visible = false;
            after_mobile_phone_edit.Visible = true;
            txt_mobile_phone.Text = "";
        }
        protected void btn_mobile_phone_edit_ok(object sender, EventArgs e)
        {
            before_mobile_phone_edit.Visible = true;
            after_mobile_phone_edit.Visible = false;
            
            if (txt_mobile_phone.Text != "")
            {
                string update_mobile_phone = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Mobile_Phone ='" + txt_mobile_phone.Text + "' where User_Name = @user_name ";
                SqlCommand cmd_mobile_phone = new SqlCommand(update_mobile_phone, con);
                con.Open();
                try
                {
                    cmd_mobile_phone.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_mobile_phone.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_mobile_phone();", true);
                    lbl_mobile_phone.Text = txt_mobile_phone.Text;
                    
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_mobile_phone();", true);
                    before_password_edit.Visible = false;
                    after_password_edit.Visible = true;
                    

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_changed_mobile_phone();", true);
            }
        }

        protected void btn_mobile_phone_edit_cancel(object sender, EventArgs e)
        {
            before_mobile_phone_edit.Visible = true;
            after_mobile_phone_edit.Visible = false;
        }
        /*Company Edit*/
        protected void btn_before_company_edit(object sender, EventArgs e)
        {
            before_company_edit.Visible = false;
            after_company_edit.Visible = true;
            txt_company.Text = "";
        }
        protected void btn_company_edit_ok(object sender, EventArgs e)
        {
            before_company_edit.Visible = true;
            after_company_edit.Visible = false;
            
            if (txt_company.Text != "")
            {
                string update_company = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Company ='" + txt_company.Text + "' where User_Name = @user_name ";
                SqlCommand cmd_company = new SqlCommand(update_company, con);
                con.Open();
                try
                {
                    cmd_company.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_company.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_company();", true);
                    lbl_company.Text = txt_company.Text;
                    
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_company();", true);
                    before_password_edit.Visible = false;
                    after_password_edit.Visible = true;
                    

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_changed_company();", true);
            }
        }

        protected void btn_company_edit_cancel(object sender, EventArgs e)
        {
            before_company_edit.Visible = true;
            after_company_edit.Visible = false;
        }
        /*Country Edit*/
        protected void btn_before_country_edit(object sender, EventArgs e)
        {
            before_country_edit.Visible = false;
            after_country_edit.Visible = true;
            txt_country.Text = "";
        }
        protected void btn_country_edit_ok(object sender, EventArgs e)
        {
            before_country_edit.Visible = true;
            after_country_edit.Visible = false;
            if (txt_country.Text != "")
            {
                string update_country = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Country ='" + txt_country.Text + "' where User_Name = @user_name ";
                SqlCommand cmd_country = new SqlCommand(update_country, con);
                con.Open();
                try
                {
                    cmd_country.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_country.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_country();", true);
                    lbl_country.Text = txt_country.Text;
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_country();", true);
                    before_password_edit.Visible = false;
                    after_password_edit.Visible = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_changed_country();", true);
            }
        }

        protected void btn_country_edit_cancel(object sender, EventArgs e)
        {
            before_country_edit.Visible = true;
            after_country_edit.Visible = false;
        }
        /*City Edit*/
        protected void btn_before_city_edit(object sender, EventArgs e)
        {
            before_city_edit.Visible = false;
            after_city_edit.Visible = true;
            txt_city.Text ="";
        }
        protected void btn_city_edit_ok(object sender, EventArgs e)
        {
            before_city_edit.Visible = true;
            after_city_edit.Visible = false;
            if (txt_city.Text != "")
            {
                string update_city = "UPDATE [PDA_CALCULATION].[dbo].[users] SET City ='" + txt_city.Text + "' where User_Name = @user_name ";
                SqlCommand cmd_city = new SqlCommand(update_city, con);
                con.Open();
                try
                {
                    cmd_city.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_city.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_country();", true);
                    lbl_city.Text = txt_city.Text;
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_country();", true);
                    before_password_edit.Visible = false;
                    after_password_edit.Visible = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_changed_country();", true);
            }
        }

        protected void btn_city_edit_cancel(object sender, EventArgs e)
        {
            before_city_edit.Visible = true;
            after_city_edit.Visible = false;
        }
        /*District Edit*/
        protected void btn_before_district_edit(object sender, EventArgs e)
        {
            before_district_edit.Visible = false;
            after_district_edit.Visible = true;
            txt_district.Text = "";
        }
        protected void btn_district_edit_ok(object sender, EventArgs e)
        {
            before_district_edit.Visible = true;
            after_district_edit.Visible = false;

            if (txt_district.Text != "")
            {
                string update_district = "UPDATE [PDA_CALCULATION].[dbo].[users] SET District ='" + txt_district.Text + "' where User_Name = @user_name ";
                SqlCommand cmd_district = new SqlCommand(update_district, con);
                con.Open();
                try
                {
                    cmd_district.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_district.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_district();", true);
                    lbl_district.Text = txt_district.Text;
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_district();", true);
                    before_password_edit.Visible = false;
                    after_password_edit.Visible = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_changed_district();", true);
            }
        }

        protected void btn_district_edit_cancel(object sender, EventArgs e)
        {
            before_district_edit.Visible = true;
            after_district_edit.Visible = false;
        }
        /*Street Edit*/
        protected void btn_before_street_edit(object sender, EventArgs e)
        {
            before_street_edit.Visible = false;
            after_street_edit.Visible = true;
            txt_street.Text = "";

        }
        protected void btn_street_edit_ok(object sender, EventArgs e)
        {
            before_street_edit.Visible = true;
            after_street_edit.Visible = false;

            if (txt_street.Text != "")
            {
                string update_street = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Street ='" + txt_street.Text + "' where User_Name = @user_name ";
                SqlCommand cmd_street = new SqlCommand(update_street, con);
                con.Open();
                try
                {
                    cmd_street.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_street.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_street();", true);
                    lbl_street.Text = txt_street.Text;
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_street();", true);
                    before_password_edit.Visible = false;
                    after_password_edit.Visible = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_changed_street();", true);
            }
        }

        protected void btn_street_edit_cancel(object sender, EventArgs e)
        {
            before_street_edit.Visible = true;
            after_street_edit.Visible = false;
        }
        /*Office Edit*/
        protected void btn_before_office_edit(object sender, EventArgs e)
        {
            before_office_edit.Visible = false;
            after_office_edit.Visible = true;
            txt_office.Text = "";
        }
        protected void btn_office_edit_ok(object sender, EventArgs e)
        {
            before_office_edit.Visible = true;
            after_office_edit.Visible = false;
           
            if (txt_office.Text != "")
            {
                string update_office = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Office ='" + txt_office.Text + "' where User_Name = @user_name ";
                SqlCommand cmd_office = new SqlCommand(update_office, con);
                con.Open();
                try
                {
                    cmd_office.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_office.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_office();", true);
                    lbl_office.Text = txt_office.Text;
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_office();", true);
                    before_password_edit.Visible = false;
                    after_password_edit.Visible = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_changed_office();", true);
            }
        }

        protected void btn_office_edit_cancel(object sender, EventArgs e)
        {
            before_office_edit.Visible = true;
            after_office_edit.Visible = false;
        }
        /*No Edit*/
        protected void btn_before_no_edit(object sender, EventArgs e)
        {
            before_no_edit.Visible = false;
            after_no_edit.Visible = true;
            txt_no.Text = "";
        }
        protected void btn_no_edit_ok(object sender, EventArgs e)
        {
            before_no_edit.Visible = true;
            after_no_edit.Visible = false;
            if (txt_no.Text != "")
            {
                string update_no = "UPDATE [PDA_CALCULATION].[dbo].[users] SET No ='" + txt_no.Text + "' where User_Name = @user_name ";
                SqlCommand cmd_no = new SqlCommand(update_no, con);
                con.Open();
                try
                {
                    cmd_no.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_no.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_no();", true);
                    lbl_no.Text = txt_no.Text;
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_no();", true);
                    before_password_edit.Visible = false;
                    after_password_edit.Visible = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_changed_no();", true);
            }
        }

        protected void btn_no_edit_cancel(object sender, EventArgs e)
        {
            before_no_edit.Visible = true;
            after_no_edit.Visible = false;
        }
        /*Logo Edit*/

        protected void before_btn_logo_edit(object sender, EventArgs e)
        {
            before_logo_edit.Visible = false;
            after_logo_edit.Visible = true;
            new_image.ImageUrl = logo;
            
        }
        protected void after_btn_logo_edit(object sender, EventArgs e)
        {

            string path = "~/img/" + logo_upload.FileName;

            if (this.logo_upload.FileName != string.Empty)
            {
                con.Close();
                logo_upload.SaveAs(Server.MapPath(path).ToString());
                new_image.ImageUrl = path;
                string update_logo = "UPDATE [PDA_CALCULATION].[dbo].[users] SET Image_Path ='" + path + "' where User_Name = @user_name ";
                SqlCommand cmd_logo = new SqlCommand(update_logo, con);
                con.Open();
                try
                {
                    cmd_logo.Parameters.AddWithValue("@user_name", Session["users"].ToString());
                    cmd_logo.ExecuteNonQuery();
                    con.Close();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_changed_logo();", true);
                    Page_Load(sender, e);
                    
                    
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_changed_logo();", true);
                    before_logo_edit.Visible = false;
                    after_logo_edit.Visible = true;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "invalid_changed_logo();", true);
            }
            
        }

        protected void btn_logo_edit_cancel(object sender, EventArgs e)
        {
            before_logo_edit.Visible = true;
            after_logo_edit.Visible = false;

        }
       
        private void sendcode()
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.Credentials = new System.Net.NetworkCredential("ylmzalper9427@gmail.com", "Alper272727.");
            smtp.EnableSsl = true;
            MailMessage msg = new MailMessage();
            msg.Subject = "Activation Code to Verify Email Adress";
            msg.Body = "Dear " + Session["users"] +",\n\n" + "Your Activation code is " +activitioncode+"\n\n\nThanks & Best Regards" + "\nAYPROFORMA";
            string toaddres = txt_email_edit.Text;
            msg.To.Add(toaddres);
            string fromaddress = "ylmzalper9427@gmail.com";
            msg.From = new MailAddress(fromaddress);

            try
            {
                smtp.Send(msg);
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void timer_activation_code_Tick(object sender, EventArgs e)
        {
            
        }

    }

}