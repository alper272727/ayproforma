﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;


namespace ayproforma
{
    public partial class login : System.Web.UI.Page
    {   

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[0].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            txt_password.Attributes["type"] = "password";
            object users = Session["users"];

            if (users != null)
            {
                System.Threading.Thread.Sleep(2000);
                Response.Redirect("home_page.aspx");  
            }
        }

        protected void btn_login_Click(object sender, EventArgs e)
        {
                      if (txt_user_name.Text != "" && txt_password.Text != "" ){

                string sorgu = "SELECT * FROM [PDA_CALCULATION].[dbo].[users] where User_Name= @kullanici_adi AND Password = @sifre";

                SqlCommand cmd = new SqlCommand(sorgu, con);

                try
                {
                    string user_name = txt_user_name.Text;
                    string password = txt_password.Text;
                    cmd.Parameters.AddWithValue("@kullanici_adi", user_name);
                    cmd.Parameters.AddWithValue("@sifre", password);
                    con.Open();

                     SqlDataReader dr = cmd.ExecuteReader();
                    
                    if (dr.Read())
                    {
                        Session.Timeout = 300;
                        Session.Add("users", dr["User_Name"].ToString());
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "login_not_match();", true);
                    }
                    con.Close();
                   
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "login_not_match();", true);

                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "login_not_blank();", true);
            }           
        }

        protected void btn_send_Click(object sender, EventArgs e)
        {
            GetIP();

            string body =  @"<tr><td>" + "Ad      :" +     "<b>" + txt_name.Text    + "</b>"+"</td></tr>" +
                             "<tr><td>" + "Soyad   :" + "<b>" + txt_surname.Text + "</b>" + "</td></tr>" +
                             "<tr><td>" + "Telefon :" + "<b>" + txt_telefon.Text + "</b>" + "</td></tr>" +
                             "<tr><td>" + "E-Mail  :" + "<b>" + txt_email.Text + "</b>" + "</td></tr>" +
                             "<tr><td>" + "Mesaj   :" + "<b>" + Request.Form["message"] + "</b>" + "</td></tr>" +
                             "<tr><td>" + "IP Address   :" + "<b>" + GetIP().ToString() + "</b>" + "</td></tr>"; 
    
            
            string subject = "Yeni Bir Müşteri Adayı Var!";

            try
            {
                string email = "ylmzalper9427@gmail.com";
                MailMessage message = new MailMessage(email, email, subject, body);
                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("ylmzalper9427@gmail.com", "Alper272727.");
                client.Send(message);
                

                /*Gönderilen mesajları tabloda kayıt altına almak için*/
                string sorgu_kontakt = "INSERT INTO [PDA_CALCULATION].[dbo].[contact] (name, surname, email, phone, message) Values (@name, @surname, @email, @phone, @message)";

                SqlCommand cmd_contact = new SqlCommand(sorgu_kontakt, con);
                con.Open();

                try
                {
                    cmd_contact.Parameters.AddWithValue("@name", txt_name.Text);
                    cmd_contact.Parameters.AddWithValue("@surname", txt_surname.Text);
                    cmd_contact.Parameters.AddWithValue("@email", txt_email.Text);
                    cmd_contact.Parameters.AddWithValue("@phone", txt_telefon.Text);
                    cmd_contact.Parameters.AddWithValue("@message", Request.Form["message"]);

                    cmd_contact.ExecuteNonQuery();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "success_send_contact();", true);
                    con.Close();
                    txt_name.Text = "";
                    txt_surname.Text = "";
                    txt_email.Text = "";
                    txt_telefon.Text = "";
                    Request.Form["message"] = "";


                }
                catch (Exception) { }

            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "alert_send_contact();", true);
            }
            /*Kayıt altına alma işlemi bittiği yer*/

        }

        public static String GetIP()
        {
            String ip =
                HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            return ip;
        }

        protected void forgot_pasword_Click(object sender, EventArgs e)
        {
            Response.Redirect("forget_password.aspx");
        }
    }
}
  